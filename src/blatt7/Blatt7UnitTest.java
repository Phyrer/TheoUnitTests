/* ======================== WICHTIG!!! ===========================
 * DENKT DRINGEND DRAN ASSERTIONS ANZUSCHALTEN!
 * dies macht man indem man das flag "-ea" an die VM uebergibt.
 */


package blatt7;

import java.sql.SQLOutput;
import java.util.HashSet;
import java.util.Set;

public class Blatt7UnitTest {
    private static void runTestA_1() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        nonTerms.add('A');
        nonTerms.add('B');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", "AB"));
        productions.add(new Production("S", ""));
        productions.add(new Production("S", "A"));
        productions.add(new Production("S", "B"));
        productions.add(new Production("A", "aAb"));
        productions.add(new Production("A", "ab"));
        productions.add(new Production("B", "Ba"));
        productions.add(new Production("B", "a"));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert CanProduce.canProduce(g, "");
        assert CanProduce.canProduce(g, "aabba");
        assert CanProduce.canProduce(g, "aabb");
        assert CanProduce.canProduce(g, "abaaaaaaa");
        assert !CanProduce.canProduce(g, "abba");
    }

    private static void runTestA_2() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        nonTerms.add('A');
        nonTerms.add('B');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", "AB"));
        productions.add(new Production("A", "AB"));
        productions.add(new Production("A", ""));
        productions.add(new Production("B", ""));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert CanProduce.canProduce(g, "");
    }

    private static void runTestA_3() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('A');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        alphabet.add('c');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("A", "a"));
        productions.add(new Production("A", "b"));
        productions.add(new Production("A", "c"));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'A');
        assert CanProduce.canProduce(g, "a");
        assert CanProduce.canProduce(g, "b");
        assert CanProduce.canProduce(g, "c");
        assert !CanProduce.canProduce(g, "aa");
    }

    private static void runTestA_4() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('[');
        alphabet.add(']');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", "[S]"));
        productions.add(new Production("S", "SS"));
        productions.add(new Production("S", ""));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert CanProduce.canProduce(g, "[][][]");
        assert !CanProduce.canProduce(g, "[[");
        assert CanProduce.canProduce(g, "[[]]");
        assert CanProduce.canProduce(g, "");
    }

    private static void runTestA_5() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        nonTerms.add('A');
        nonTerms.add('B');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", ""));
        productions.add(new Production("S", "AB"));
        productions.add(new Production("S", "ASB"));
        productions.add(new Production("A", "a"));
        productions.add(new Production("B", "b"));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "a");
        assert !CanProduce.canProduce(g, "b");
        assert CanProduce.canProduce(g, "ab");
        assert CanProduce.canProduce(g, "aabb");
        assert !CanProduce.canProduce(g, "ba");
        assert !CanProduce.canProduce(g, "abb");
        assert !CanProduce.canProduce(g, "baba");
    }

    private static void runTestA_6() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        nonTerms.add('A');
        nonTerms.add('B');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", "A"));
        productions.add(new Production("A", "S"));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert !CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "a");
        assert !CanProduce.canProduce(g, "b");
        assert !CanProduce.canProduce(g, "ab");
        assert !CanProduce.canProduce(g, "aabb");
        assert !CanProduce.canProduce(g, "ba");
        assert !CanProduce.canProduce(g, "abb");
        assert !CanProduce.canProduce(g, "baba");
    }

    private static void runTestA_7() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        nonTerms.add('A');
        nonTerms.add('B');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", "A"));
        productions.add(new Production("A", "S"));
        productions.add(new Production("B", "S"));
        productions.add(new Production("B", ""));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert !CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "a");
    }

    private static void runTestA_8() {
        HashSet<Character> nonTerms = new HashSet<>();
        nonTerms.add('S');
        nonTerms.add('A');
        nonTerms.add('B');
        HashSet<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        HashSet<Production> productions = new HashSet<>();
        productions.add(new Production("S", "AB"));
        productions.add(new Production("A", "aAA"));
        productions.add(new Production("A", "B"));
        productions.add(new Production("B", "bBS"));
        productions.add(new Production("B", ""));
        Grammar g = new Grammar(alphabet, nonTerms, productions, 'S');
        assert CanProduce.canProduce(g, "");
        assert CanProduce.canProduce(g, "a");
        assert CanProduce.canProduce(g, "abba");
        assert CanProduce.canProduce(g, "abbbb");
    }

    private static void runTestA_9() {
        Set<Character> variables = new HashSet<>();
        variables.add('A');

        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        alphabet.add('c');

        Set<Production> productions = new HashSet<>();
        productions.add(new Production("A", "a"));
        productions.add(new Production("A", "b"));
        productions.add(new Production("A", "c"));

        Grammar g = new Grammar(alphabet, variables, productions, 'A');
        assert (CanProduce.canProduce(g, "a"));
        assert (CanProduce.canProduce(g, "b"));
        assert (CanProduce.canProduce(g, "c"));
        assert !(CanProduce.canProduce(g, "aa"));
        assert !(CanProduce.canProduce(g, ""));
    }

    private static void runTestA_10() {
        Set<Character> variables = new HashSet<>();
        variables.add('A');
        variables.add('B');
        variables.add('S');

        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');

        Set<Production> productions = new HashSet<>();
        productions.add(new Production("B", "b"));
        productions.add(new Production("S", "ASB"));
        productions.add(new Production("S", "AB"));
        productions.add(new Production("A", "a"));
        productions.add(new Production("S", ""));

        Grammar g = new Grammar(alphabet, variables, productions, 'S');
        assert CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "a");
        assert !CanProduce.canProduce(g, "b");
        assert CanProduce.canProduce(g, "ab");
        assert CanProduce.canProduce(g, "aabb");
        assert !CanProduce.canProduce(g, "ba");
        assert !CanProduce.canProduce(g, "abb");
        assert !CanProduce.canProduce(g, "baba");
    }

    private static void runTestA_11() {

        Set<Character> variables = new HashSet<>();
        variables.add('A');
        variables.add('B');
        variables.add('C');
        variables.add('D');
        variables.add('S');

        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        alphabet.add('c');
        alphabet.add('d');

        Set<Production> productions = new HashSet<>();
        productions.add(new Production("S", "ABCD"));
        productions.add(new Production("S", "a"));
        productions.add(new Production("A", ""));
        productions.add(new Production("B", "b"));
        productions.add(new Production("C", "c"));
        productions.add(new Production("D", "d"));

        Grammar g = new Grammar(alphabet, variables, productions, 'S');

        assert CanProduce.canProduce(g, "a");
        assert CanProduce.canProduce(g, "bcd");

        assert !CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "ab");
    }

    private static void runTestA_12() {
        Set<Character> variables = new HashSet<>();
        variables.add('S');
        variables.add('A');
        variables.add('B');

        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');

        Set<Production> productions = new HashSet<>();
        productions.add(new Production("S", "ASA"));
        productions.add(new Production("S", "aB"));
        productions.add(new Production("A", "B"));
        productions.add(new Production("A", "S"));
        productions.add(new Production("B", "b"));
        productions.add(new Production("B", ""));

        Grammar g = new Grammar(alphabet, variables, productions, 'S');

        assert CanProduce.canProduce(g, "a");
        assert CanProduce.canProduce(g, "ab");
        assert CanProduce.canProduce(g, "bab");
        assert CanProduce.canProduce(g, "aa");

        assert !CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "b");
    }

    private static void runTestA_13() {
        Set<Character> variables = new HashSet<>();
        variables.add('S');
        variables.add('A');
        variables.add('B');
        variables.add('C');
        variables.add('X');

        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');

        Set<Production> productions = new HashSet<>();
        productions.add(new Production("S", "AXBXa"));
        productions.add(new Production("A", "C"));
        productions.add(new Production("C", "a"));
        productions.add(new Production("B", "b"));
        productions.add(new Production("X", ""));
        productions.add(new Production("X", "S"));

        Grammar g = new Grammar(alphabet, variables, productions, 'S');

        assert CanProduce.canProduce(g, "aba");
        assert CanProduce.canProduce(g, "aababa");

        assert !CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "a");
        assert !CanProduce.canProduce(g, "b");
    }

    private static void runTestA_14() {
        Set<Character> variables = new HashSet<>();
        variables.add('S');
        variables.add('A');

        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');

        Set<Production> productions = new HashSet<>();
        productions.add(new Production("S", "AaAA"));
        productions.add(new Production("S", "S"));
        productions.add(new Production("A", ""));
        productions.add(new Production("A", "a"));

        char startingSymbol = 'S';

        Grammar g = new Grammar(alphabet, variables, productions, startingSymbol);

        assert CanProduce.canProduce(g, "a");
        assert CanProduce.canProduce(g, "aa");
        assert CanProduce.canProduce(g, "aaa");
        assert CanProduce.canProduce(g, "aaaa");

        assert !CanProduce.canProduce(g, "");
        assert !CanProduce.canProduce(g, "aaaaaaa");
        assert !CanProduce.canProduce(g, "b");
    }

    private static void runTestsA() {
        System.out.print("Running tests for Task A...");
        runTestA_1();
        runTestA_2();
        runTestA_3();
        runTestA_4();
        runTestA_5();
        runTestA_6();
        runTestA_7();
        runTestA_8();
        runTestA_9();
        runTestA_10();
        runTestA_11();
        runTestA_12();
        runTestA_13();
        runTestA_14();
        System.out.println("Done");
    }

    private static void runTestB_1() {
        Set<Character> alphabet = new HashSet<>();
        alphabet.add('0');
        alphabet.add('1');
        State[] states = new State[5];
        Set<State> stateSet = new HashSet<>();
        for (int i = 0; i < states.length; i++) {
            states[i] = new State();
            stateSet.add(states[i]);
        }
        Set<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(states[0], states[1], '0'));
        transitions.add(new Transition(states[0], states[2], '1'));
        transitions.add(new Transition(states[1], states[4], '0'));
        transitions.add(new Transition(states[1], states[2], '1'));
        transitions.add(new Transition(states[2], states[3], '0'));
        transitions.add(new Transition(states[2], states[2], '1'));
        transitions.add(new Transition(states[3], states[4], '0'));
        transitions.add(new Transition(states[3], states[0], '1'));
        transitions.add(new Transition(states[4], states[4], '0'));
        transitions.add(new Transition(states[4], states[4], '1'));

        Set<State> finalStates = new HashSet<>();
        finalStates.add(states[4]);

        DFA d = new DFA(stateSet, transitions, alphabet, states[0], finalStates);
        String out = Minimize.minimize(d).toString();
        String comp = "DFA\n" +
                "Alphabet: 0;1\n" +
                "States: s5;s6;s7\n" +
                "Init: s5\n" +
                "Final: s7\n" +
                "Transitions:\n" +
                "s5;0;s6\n" +
                "s5;1;s5\n" +
                "s6;0;s7\n" +
                "s6;1;s5\n" +
                "s7;0;s7\n" +
                "s7;1;s7\n" +
                "END";
        assert out.equals(comp);
    }

    private static void runTestB_2() {
        Set<Character> alphabet = new HashSet<>();
        alphabet.add('0');
        alphabet.add('1');
        State[] states = new State[6];
        Set<State> stateSet = new HashSet<>();
        for (int i = 0; i < states.length; i++) {
            states[i] = new State();
            stateSet.add(states[i]);
        }
        Set<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(states[0], states[3], '0'));
        transitions.add(new Transition(states[0], states[1], '1'));
        transitions.add(new Transition(states[1], states[2], '0'));
        transitions.add(new Transition(states[1], states[5], '1'));
        transitions.add(new Transition(states[2], states[2], '0'));
        transitions.add(new Transition(states[2], states[5], '1'));
        transitions.add(new Transition(states[3], states[0], '0'));
        transitions.add(new Transition(states[3], states[4], '1'));
        transitions.add(new Transition(states[4], states[2], '0'));
        transitions.add(new Transition(states[4], states[5], '1'));
        transitions.add(new Transition(states[5], states[5], '0'));
        transitions.add(new Transition(states[5], states[5], '1'));


        Set<State> finalStates = new HashSet<>();
        finalStates.add(states[1]);
        finalStates.add(states[2]);
        finalStates.add(states[4]);

        DFA d = new DFA(stateSet, transitions, alphabet, states[0], finalStates);
        String out = Minimize.minimize(d).toString();
        String comp = "DFA\n" +
                "Alphabet: 0;1\n" +
                "States: s14;s15;s16\n" +
                "Init: s14\n" +
                "Final: s15\n" +
                "Transitions:\n" +
                "s14;0;s14\n" +
                "s14;1;s15\n" +
                "s15;0;s15\n" +
                "s15;1;s16\n" +
                "s16;0;s16\n" +
                "s16;1;s16\n" +
                "END";
        assert out.equals(comp);
    }

    private static void runTestB_3() {
        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        alphabet.add('b');
        State[] states = new State[6];
        Set<State> stateSet = new HashSet<>();
        for (int i = 0; i < states.length; i++) {
            states[i] = new State();
            stateSet.add(states[i]);
        }
        Set<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(states[0], states[1], 'a'));
        transitions.add(new Transition(states[0], states[2], 'b'));
        transitions.add(new Transition(states[1], states[0], 'a'));
        transitions.add(new Transition(states[1], states[3], 'b'));
        transitions.add(new Transition(states[2], states[4], 'a'));
        transitions.add(new Transition(states[2], states[5], 'b'));
        transitions.add(new Transition(states[3], states[4], 'a'));
        transitions.add(new Transition(states[3], states[5], 'b'));
        transitions.add(new Transition(states[4], states[4], 'a'));
        transitions.add(new Transition(states[4], states[5], 'b'));
        transitions.add(new Transition(states[5], states[5], 'a'));
        transitions.add(new Transition(states[5], states[5], 'b'));


        Set<State> finalStates = new HashSet<>();
        finalStates.add(states[2]);
        finalStates.add(states[3]);
        finalStates.add(states[4]);

        DFA d = new DFA(stateSet, transitions, alphabet, states[0], finalStates);
        String out = Minimize.minimize(d).toString();
        String comp = "DFA\n" +
                "Alphabet: a;b\n" +
                "States: s23;s24;s25\n" +
                "Init: s23\n" +
                "Final: s25\n" +
                "Transitions:\n" +
                "s23;a;s23\n" +
                "s23;b;s25\n" +
                "s24;a;s24\n" +
                "s24;b;s24\n" +
                "s25;a;s25\n" +
                "s25;b;s24\n" +
                "END";
        assert out.equals(comp);
    }

    private static void runTestB_4() {
        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        State[] states = new State[1];
        Set<State> stateSet = new HashSet<>();
        for (int i = 0; i < states.length; i++) {
            states[i] = new State();
            stateSet.add(states[i]);
        }
        Set<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(states[0], states[0], 'a'));


        Set<State> finalStates = new HashSet<>();
        finalStates.add(states[0]);

        DFA d = new DFA(stateSet, transitions, alphabet, states[0], finalStates);
        String out = Minimize.minimize(d).toString();
        String comp = "DFA\n" +
                "Alphabet: a\n" +
                "States: s27\n" +
                "Init: s27\n" +
                "Final: s27\n" +
                "Transitions:\n" +
                "s27;a;s27\n" +
                "END";
        assert out.equals(comp);
    }

    private static void runTestB_5() {
        Set<Character> alphabet = new HashSet<>();
        alphabet.add('a');
        State[] states = new State[3];
        Set<State> stateSet = new HashSet<>();
        for (int i = 0; i < states.length; i++) {
            states[i] = new State();
            stateSet.add(states[i]);
        }
        Set<Transition> transitions = new HashSet<>();
        transitions.add(new Transition(states[0], states[1], 'a'));
        transitions.add(new Transition(states[1], states[1], 'a'));
        transitions.add(new Transition(states[2], states[2], 'a'));


        Set<State> finalStates = new HashSet<>();
        finalStates.add(states[2]);

        DFA d = new DFA(stateSet, transitions, alphabet, states[0], finalStates);
        String out = Minimize.minimize(d).toString();
        String comp = "DFA\n" +
                "Alphabet: a\n" +
                "States: s31\n" +
                "Init: s31\n" +
                "Final:\n" +
                "Transitions:\n" +
                "s31;a;s31\n" +
                "END";
        assert out.equals(comp);
    }

    private static void runTestsB() {
        /* ERGEBNISSE STIMMEN NICHT UNBEDINGT MIT DEN HIER GETESTETEN UEBEREIN
         * hier werden vor allem die namen der states verglichen, andere
         * Ansaetze koennten andere Namen erzeugen. Vergleicht bei falschen
         * Ergebnissen den "out" String mit einem Debugger und gleicht ggf
         * den "comp" String an.
         * DIE FEHLER PFLANZEN SICH DURCH DIE TESTS FORT!!!
         */
        System.out.print("Running tests for Task B...");
        runTestB_1();
        runTestB_2();
        runTestB_3();
        runTestB_4();
        runTestB_5();
        System.out.println("Done");
    }

    public static void main(String[] args) {
        // check whether assertions are enabled
        boolean assertOn = false;
        assert assertOn = true;
        if (!assertOn) {
            throw new RuntimeException("Assertions are disabled");
        }
        runTestsA();
        runTestsB();
    }
}
